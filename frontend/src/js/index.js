import React from 'react';
import ReactDOM from 'react-dom';
import '../css/app.css';
import App from '../components/App';

ReactDOM.render(<App />, document.getElementById('app'));