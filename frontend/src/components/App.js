import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import "../css/foundation.css"
import "../css/app.css"

import Navbar from "./Navbar.component";
import AllBooksList from './AllBooksList.component';
import MyBackStack from './MyBackStack.component';
import MyBooksList from './MyBooksList.component';
import MyShelvesList from './MyShelvesList.component';

function App() {
  return (
    <Router>
      <div className="grid-container">
        <Navbar />
        <br/>
        <Route path="/all" exact component={AllBooksList} />
        <Route path="/my-books" exact component={MyBooksList} />
        <Route path="/my-shelves" exact component={MyShelvesList} />
        <Route path="/backstack" exact component={MyBackStack} />
      </div>
    </Router>
  );
}

export default App;