import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Navbar extends Component {

  render() {
    return (
      <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
        <Link to="/" className="navbar-brand">Welcome to BackStacks!</Link>
        <ul className="menu expanded">
          <li className="navbar-item">
          <Link to="/all" className="nav-link">All Books</Link>
          </li>
          <li className="navbar-item">
          <Link to="/my-books" className="nav-link">My Books</Link>
          </li>
          <li className="navbar-item">
          <Link to="/my-shelves" className="nav-link">My Shelves</Link>
          </li>
          <li className="navbar-item">
          <Link to="/backstack" className="nav-link">My BackStack</Link>
          </li>
        </ul>
      </nav>
    );
  }
}