const router = require('express').Router();
let Book = require('../models/book.model');

router.route('/allBooks').get((req, res) => {
    Book.find()
        .then(books => res.json(books))
        .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;