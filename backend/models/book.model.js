const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const volumeInfoSchema = new Schema({
    title: {type: String, required: true },
    subtitle: {type: String},
    authors: {type: Array},
    publisher: {type: String},
    publishedDate: {type: String},
    description: {type: String},
    industryIdentifiers: {type: Array},
    readingModes: {type: Object},
    pageCount: {type: Number},
    printType: {type: String},
    categories: {type: Array},
    maturityRating: {type: String},
    allowAnonLogging: {type: Boolean},
    contentVersion: {type: String},
    imageLinks: {type: Object},
    language: {type: String},
    previewLink: {type: String},
    infoLink: {type: String},
    canonicalVolumeLink: {type: String}
});

const itemsSchema = new Schema({
    kind: {type: String},
    id: {type: String},
    etag: {type: String},
    selfLink: {type: String},
    volumeInfo: [volumeInfoSchema]
});

const bookSchema = new Schema({
    items: [itemsSchema]
});

const Book = mongoose.model('Book', bookSchema);

module.exports = Book