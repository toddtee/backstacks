import os 
import requests
import json

URL =  'https://www.googleapis.com/books/v1/volumes/'
API_KEY = os.environ.get('GBOOKSAPIKEY')

def create_library_data(isbn_list_file):
    isbn_list = open_book_list(isbn_list_file)
    library_data_file = create_library_file()
    for isbn in isbn_list:
        volume = get_volume_from_isbn(isbn)
        if volume['totalItems'] == 0:
            volume["isbn"] =str(isbn)
            library_data_file.write(json.dumps(volume, indent=2)+'\n')
        elif volume['totalItems'] > 0:
            library_data_file.write(json.dumps(volume, indent=2)+'\n')
    close_library_file(library_data_file) 

def open_book_list(book_list):
    x = []
    with open(book_list, 'r') as bl:
        for line in bl:
            x.append(line)
    
    return x

def create_library_file():
    library_data_file = open('my_library.json', 'w')

    return library_data_file

def close_library_file(library_data_file):
    library_data_file.close()

def get_volume_from_isbn(isbn):
    v = URL + '?q=isbn:' + isbn + "&key=" + API_KEY
    r = requests.get(v)

    volume = json.loads(r.content)

    return volume

def get_volume_from_id(volume_id):
    v = URL + volume_id
    r = requests.get(v)

    return r

if __name__ == "__main__":
    create_library_data('isbn.txt')