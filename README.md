# Hi There!

## Welcome to BackStacks!

* This will be a web-based app that will track and manage my reading backlog.
* I am going to create a simple database first which will hold my reading backlog items with various metadata for each item.
* Create a web front end to view this backlog.
* Management, sorting, filtering and other fun stats will be added as time goes on.
* Proposing to try some new tech/processes where I can (nosql with mongodb, implement CI/CD, etc).
* Project will display my ability to develop and manage a project.
* Aim to grow my front end abilities.